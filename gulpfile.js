const {src, dest} = require('gulp')


function css() {
  return src('static/sass/*.{scss,sass}').pipe(dest('static/css'))
}

exports.css = css